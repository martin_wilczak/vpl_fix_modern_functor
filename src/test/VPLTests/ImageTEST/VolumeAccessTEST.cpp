//==============================================================================
/* This file comes from MDSTk software and was modified for
 * 
 * VPL - Voxel Processing Library
 * Changes are Copyright 2014 3Dim Laboratory s.r.o.
 * All rights reserved.
 * 
 * Use of this source code is governed by a BSD-style license that can be
 * found in the LICENSE file.
 * 
 * The original MDSTk legal notice can be found below.
 * 
 * Medical Data Segmentation Toolkit (MDSTk) 
 * Copyright (c) 2003-2005 by Michal Spanel  
 *
 * Author:  Michal Spanel, spanel@fit.vutbr.cz  \n
 * Date:    2004/06/01                       
 *
 * Description:
 * - Testing of the vpl::CVolume template.
 */

#include <VPL/Base/PartedData.h>
#include <VPL/Image/Image.h>
#include <VPL/Image/Volume.h>
#include <VPL/Image/VolumeFunctions.h>
#include <VPL/Module/Serialization.h>

// STL
#include <iostream>
#include <string>
#include <ctime>

// Default allocator
//typedef vpl::img::CVolume<vpl::img::tDensityPixel> tVolume;

// Parted allocator
typedef vpl::img::CVolume<vpl::img::tDensityPixel, vpl::base::CPartedData> tVolume;

typedef tVolume::tSmartPtr tVolumePtr;


//==============================================================================
/*!
 * Prints a given image
 */
void printImage(vpl::img::CDImage& Image, bool bPrintMargin = false)
{
    std::cout.setf(std::ios_base::fixed);
    vpl::tSize Margin = (bPrintMargin) ? Image.getMargin() : 0;
    for( vpl::tSize j = -Margin; j < Image.getYSize() + Margin; j++ )
    {
        std::cout << "  ";
        for( vpl::tSize i = -Margin; i < Image.getXSize() + Margin; i++ )
        {
            std::cout << Image(i, j) << "  ";
        }
        std::cout << std::endl;
    }
}


//==============================================================================
/*!
 * Prints a given volume
 */
void printVolume(tVolume& Volume, bool bPrintMargin = false)
{
    static char pcPrefix[255];

    std::cout.setf(std::ios_base::fixed);
    vpl::tSize Margin = (bPrintMargin) ? Volume.getMargin() : 0;
    for( vpl::tSize j = -Margin; j < Volume.getYSize() + Margin; j++ )
    {
        for( vpl::tSize k = -Margin; k < Volume.getZSize() + Margin; k++ )
        {
            memset(pcPrefix, (int)' ', Volume.getZSize() - k + 1);
            pcPrefix[Volume.getZSize() - k + 1] = '\0';
            std::cout << pcPrefix;
            for( vpl::tSize i = -Margin; i < Volume.getXSize() + Margin; i++ )
            {
                std::cout << Volume(i, j, k) << "  ";
            }
            std::cout << std::endl << std::endl;
        }
    }
}


//==============================================================================
/*!
 * Waiting for a key
 */
void keypress()
{
    while( std::cin.get() != '\n' );
}


//==============================================================================
/*!
 * Clock counter
 */
clock_t clockCounter;


//==============================================================================
/*!
 * Starts time measuring
 */
void begin()
{
    clockCounter = clock();
}

//==============================================================================
/*!
 * Stops time measuring and prints result
 */
void end()
{
    clockCounter = clock() - clockCounter;
    std::cout << "  Measured clock ticks: " << clockCounter << std::endl;
}


//==============================================================================
/*!
 * main
 */
int main(int argc, const char *argv[])
{
    std::cout << "Testing voxel access overload" << std::endl;
    tVolume volume3(256,256,256,8);

#ifdef _DEBUG
    int c;
    const int count = 5;
#else
    int c;
    const int count = 100;
#endif

    std::cout << "  Basic version" << std::endl;
    begin();
    for (c = 0; c < count; ++c)
    {
        for (int k = 0; k < volume3.getZSize(); ++k)
        {
            for (int j = 0; j < volume3.getYSize(); ++j)
            {
                for (int i = 0; i < volume3.getXSize(); ++i)
                {
                    volume3(i, j, k) = 1234;
                }
            }
        }
    }
    end();
    keypress();

    std::cout << "  Iterator version" << std::endl;
    begin();
    for (c = 0; c < count; ++c)
    {
        ;
        for (tVolume::tIterator it(volume3); it; ++it)
        {
            *it = 1234;
        }
    }
    end();
    keypress();

    std::cout << "  Fill version" << std::endl;
    begin();
    for (c = 0; c < count; ++c)
    {
        volume3.fill(1234);
    }
    end();
    keypress();

    std::cout << "  Fill entire version" << std::endl;
    begin();
    for (c = 0; c < count; ++c)
    {
        volume3.fillEntire(1234);
    }
    end();
    keypress();


    std::cout << "Testing dummy volumes" << std::endl;
    volume3.enableDummyMode(true);
    volume3.resize(3, 3, 3, 1);
    for (int k = 0; k < volume3.getZSize(); k++)
    {
        for (int j = 0; j < volume3.getYSize(); j++)
        {
            for (int i = 0; i < volume3.getXSize(); i++)
            {
                volume3(i, j, k) = k * volume3.getYSize() * volume3.getZSize() + j * volume3.getXSize() + i;
            }
        }
    }
    printVolume(volume3, true);
    keypress();

    std::cout << "Fill margin of the dummy volume 3" << std::endl;
    volume3.fillMargin(1);
    printVolume(volume3, true);
    keypress();

    std::cout << "Testing voxel access overload for dummy volumes" << std::endl;
    volume3.resize(256, 256, 256, 8);

    std::cout << "  Basic version" << std::endl;
    begin();
    for( c = 0; c < count; ++c )
    {
        for( int k = 0; k < volume3.getZSize(); ++k )
        {
            for( int j = 0; j < volume3.getYSize(); ++j )
            {
                for( int i = 0; i < volume3.getXSize(); ++i )
                {
                    volume3(i, j, k) = 1234;
                }
            }
        }
    }
    end();
    keypress();

    std::cout << "  Iterator version" << std::endl;
    begin();
    for( c = 0; c < count; ++c )
    {
        ;
        for( tVolume::tIterator it(volume3); it; ++it )
        {
            *it = 1234;
        }
    }
    end();
    keypress();

    std::cout << "  Fill version" << std::endl;
    begin();
    for( c = 0; c < count; ++c )
    {
        volume3.fill(1234);
    }
    end();
    keypress();

    std::cout << "  Fill entire version" << std::endl;
    begin();
    for( c = 0; c < count; ++c )
    {
        volume3.fillEntire(1234);
    }
    end();
    keypress();

    return 0;
}

