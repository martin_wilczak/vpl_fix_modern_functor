//==============================================================================
/* This file is part of
*
* VPL - Voxel Processing Library
* Copyright 2014 3Dim Laboratory s.r.o.
* All rights reserved.
*
* Use of this source code is governed by a BSD-style license that can be
* found in the LICENSE file.
*/

#include "gtest/gtest.h"
#include <VPL/Math/Vector.h>
#include <VPL/Math/VectorFunctions.h>
#include "VPL/Test/Utillities/arguments.h"
#include "VPL/Test/Compare/compare1D.h"

namespace vector
{
//! Test fixture
template<class T>
class VectorTest : public testing::Test
{
public:
    typedef  T type;

    //! For testing value by value, is created comparator, where: 
    //! 1 template argument type of simple value (tDensityPixel, float ...)
    //! 2 template argument is type of object which will be tested.
    //! 3 template arguments which is defauilt AccessorAt. It is class which define how is one value from object getted.
	vpl::test::Compare1D<type, vpl::math::CVector<type>> compare;

    typename vpl::math::CVector<T>::tSmartPtr spVector;
    vpl::math::CVector<T> vector1;
    vpl::math::CVector<T> vector2;

    void SetUp() override
    {
		compare.setErrorMessage("Vectors differ at index");

        spVector = typename vpl::math::CVector<T>::tSmartPtr(new typename vpl::math::CVector<T>(12));
        vector1 = vpl::math::CVector<T>(6);
        vector2 = vpl::math::CVector<T>(6);

		spVector->fill(0);
    }

	//! Generate 0,1,2,3 to vector size
    static void createSequence(vpl::math::CVector<T>& vector)
    {
        for (vpl::tSize j = 0; j < vector.size(); j++)
        {
            vector(j) = j;
        }
    }

};

//! Define types for typed testing
typedef testing::Types<int, double, float> implementations;
TYPED_TEST_CASE(VectorTest, implementations);

//! Testing base initialization
TYPED_TEST(VectorTest, Initialize)
{
	using type = typename TestFixture::type;

    vpl::math::CVector<type> v2;
    EXPECT_EQ(0, v2.size());

    vpl::math::CVector<type> v3(4);
    EXPECT_EQ(4, v3.size()) << "Vectors v3 has unequal size.";
}

//! Testing method at, it important to check, because other tests use it.
TYPED_TEST(VectorTest, At)
{
	TestFixture::vector1(0) = 5;
    EXPECT_EQ(5, TestFixture::vector1(0));
	TestFixture::vector1(0) = 0;
    EXPECT_EQ(0, TestFixture::vector1(0));
	TestFixture::vector1.at(1) = 10;
    EXPECT_EQ(10, TestFixture::vector1.at(1));
}

TYPED_TEST(VectorTest, Limit)
{
	using type = typename TestFixture::type;

    TestFixture::createSequence(*TestFixture::spVector);
    TestFixture::spVector->limit(2, 8);
	type test[] = { 2,2,2,3,4,5,6,7,8,8,8,8 };
    TestFixture::compare.values(test, *TestFixture::spVector, 12);
}


TYPED_TEST(VectorTest, Concat)
{
	using type = typename TestFixture::type;

    TestFixture::vector1.fill(1);
    TestFixture::vector2.fill(2);
    TestFixture::spVector->concat(TestFixture::vector1, TestFixture::vector2);
	type test[] = { 1,1,1,1,1,1,2,2,2,2,2,2 };
    TestFixture::compare.values(test, *TestFixture::spVector, 12);
}

TYPED_TEST(VectorTest, Fill) 
{
	using type = typename TestFixture::type;
    vpl::math::CVector<type> vector(40);
    vector.fill(100);
	TestFixture::compare.values(100, vector, 40);
}

TYPED_TEST(VectorTest, SharedPtr)
{
    EXPECT_EQ(0, (*TestFixture::spVector)(0));
}

TYPED_TEST(VectorTest, Segment)
{
	using type = typename TestFixture::type;

	TestFixture::spVector->asEigen().segment(0, 4).fill(2);
	type test[] = { 2,2,2,2,0,0,0,0,0,0,0,0 };
	TestFixture::compare.values(test, *TestFixture::spVector, 12);

	TestFixture::spVector->asEigen().segment(4, 4).fill(5);
    for (int i = 4; i < 8; i++)
    {
        test[i] = 5;
    }
	TestFixture::compare.values(test, *TestFixture::spVector, 12);
}

TYPED_TEST(VectorTest, Iterator)
{
	using type = typename TestFixture::type;

    int i = 0;
    for (typename vpl::math::CVector<type>::tIterator it(*TestFixture::spVector); it; ++it)
    {
        *it = i++;
    }

    for (vpl::tSize j = 0; j < TestFixture::spVector->size(); j++)
    {
        EXPECT_EQ(j, (*TestFixture::spVector)(j));
    }
}

TYPED_TEST(VectorTest, MinMaxAtd)
{
	using type = typename TestFixture::type;

    TestFixture::spVector->fill(0);
	type i = 0;
	type sum = 0;


    for (typename vpl::math::CVector<type>::tIterator it(*TestFixture::spVector); it; ++it)
    {
        sum += i;
        *it = i++;
    }
    EXPECT_EQ(0, vpl::math::getMin<type>(*TestFixture::spVector));
    EXPECT_EQ(11, vpl::math::getMax<type>(*TestFixture::spVector));
    EXPECT_EQ(sum, vpl::math::getSum<type>(*TestFixture::spVector));
    EXPECT_EQ(sum / 12, vpl::math::getMean<type>(*TestFixture::spVector));

}

TYPED_TEST(VectorTest, SubSample)
{
	using type = typename TestFixture::type;

    vpl::math::CVector<type> v(6);
	TestFixture::createSequence(*TestFixture::spVector);

    v.subSample(*TestFixture::spVector, 2);

	type test[] = { 0,2,4,6,8,10 };
    TestFixture::compare.values(test, v, 6);
}


TYPED_TEST(VectorTest, Add) 
{
    TestFixture::vector1.fill(100);
	TestFixture::vector2.fill(200);
	TestFixture::vector2 += TestFixture::vector1;

	TestFixture::compare.values(300, TestFixture::vector2, 6);

    TestFixture::vector1.fill(-100);
    TestFixture::vector2 += TestFixture::vector1;
    TestFixture::compare.values(200, TestFixture::vector2, 6);
}

}
