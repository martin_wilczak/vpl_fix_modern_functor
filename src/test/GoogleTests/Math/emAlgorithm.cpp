//==============================================================================
/* This file is part of
*
* VPL - Voxel Processing Library
* Copyright 2014 3Dim Laboratory s.r.o.
* All rights reserved.
*
* Use of this source code is governed by a BSD-style license that can be
* found in the LICENSE file.
*/


#include <gtest/gtest.h>
#include <VPL/Math/Vector.h>
#include "VPL/Math/Random.h"
#include "VPL/Math/Algorithm/EM.h"
#include "VPL/Test/Compare/compare1D.h"


namespace emAlgorithm
{
namespace settings
{
//! Number of input samples.
const vpl::tSize numOfSamples = 1000;
//! Gaussian parameters.
const double m1 = 0.0;
const double s1 = 5.0;
const double m2 = -30.0;
const double s2 = 5.0;
const double m3 = 50.0;
const double s3 = 10.0;

//! Required data.
const double mean[3] = { 49.9572 ,0.147837,-29.7644 };
const double cov[3] = { 99.8354 ,22.1217 ,27.5846 };
double member150[3] = { 2.83998e-07, 0.999987,1.2311e-05 };
double member450[3] = { 3.46031e-15,4.39068e-10,  1 };
double member750[3] = { 1, 2.88877e-19, 2.69982e-43 };

const double value150 = -4.39502;
const double value450 = -30.8177;
const double value750 = 44.0735;

} // namespace settings

//! Testing EmAlgorithm for clustering.
TEST(EMAlgorithmTest, Clustering)
{
    // Random numbers generator
    vpl::math::CNormalPRNG random;
    vpl::tSize third = settings::numOfSamples / 3;

    // Generate random data
    vpl::math::CDVector v1(settings::numOfSamples);
    v1.fill(1);
    for (vpl::tSize i = 0; i < settings::numOfSamples; ++i)
    {
        if (i < third)
        {
            v1(i) = random.random(settings::m1, settings::s1);
        }
        else if (i < 2 * third)
        {
            v1(i) = random.random(settings::m2, settings::s2);
        }
        else
        {
            v1(i) = random.random(settings::m3, settings::s3);
        }
    }

    vpl::math::CMaxLikelihoodByEM<vpl::math::CDVector, 1> Clustering;
    ASSERT_TRUE(Clustering.execute(v1)) << "Error: EM algorithm failed!";

    vpl::math::CMaxLikelihoodByEM<vpl::math::CDVector, 1>::tComponent c;
    ASSERT_EQ(3, Clustering.getNumOfComponents());
    for (vpl::tSize i = 0; i < Clustering.getNumOfComponents(); ++i)
    {
        c = Clustering.getComponent(i);
        ASSERT_NEAR(settings::mean[i], c.getMean().at(0), 0.001);
        ASSERT_NEAR(settings::cov[i], c.getCov().at(0), 0.001);
    }

    vpl::math::CMaxLikelihoodByEM<vpl::math::CDVector, 1>::tVector v;

    Clustering.getMembership(450, v);
    
    vpl::test::Compare1D<double, vpl::math::CVector<double>> compare;

    Clustering.getMembership(150, v);
    compare.values(settings::member150, v, v.size());
    ASSERT_NEAR(settings::value150, v1(150), 0.001);

    Clustering.getMembership(450, v);
    compare.values(settings::member450, v, v.size());
    ASSERT_NEAR(settings::value450, v1(450), 0.001);

    Clustering.getMembership(750, v);
    compare.values(settings::member750, v, v.size());
    ASSERT_NEAR(settings::value750, v1(750), 0.001);
}
} // namespace emAlgorithm
