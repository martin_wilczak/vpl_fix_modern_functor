///==============================================================================
/* This file is part of
*
* VPL - Voxel Processing Library
* Copyright 2014 3Dim Laboratory s.r.o.
* All rights reserved.
*
* Use of this source code is governed by a BSD-style license that Can be
* found in the LICENSE file.
*/

#include "gtest/gtest.h"
#include <VPL/Base/ScopedPtr.h>


//==============================================================================
/*!
* Object Ca
*/
namespace scoped
{

std::string requiredOutput;

class Ca
{
public:
    //! Allow class instantiation by smart pointer
    VPL_SCOPEDPTR(Ca);

public:
    //! Internal data
    int iData;

public:
    //! Constructor
    Ca() : iData(0)
    {
    }

    //! Constructor
	explicit Ca(const int iData) : iData(iData) {}

    //! Constructor
	explicit Ca(const int *piData) : iData(*piData){}

    //! Copy constructor
	Ca(const Ca& a) : iData(a.iData) {}

    //! Destructor
    ~Ca()
    {
		requiredOutput += "CA::~CA()#";
    }
};
//! Base pointer to the class Ca
typedef vpl::base::CScopedPtr<Ca> tCaPtr;


//! Testing using CA() default constructor
TEST(ScopedTest,ConstructorDefault)
{
	const tCaPtr p(new Ca());
    ASSERT_EQ(0, p->iData);
}
//! Testing CA(const int) constructor
TEST(ScopedTest, ConstructorArg)
{
	requiredOutput.clear();
	{
		const tCaPtr p(new Ca(10));
		ASSERT_EQ(10, p->iData);
	}

	ASSERT_STREQ("CA::~CA()#", requiredOutput.c_str());
}

//! Testing CA(const int *) constructor
TEST(ScopedTest, ConstructorArgPtr)
{
	requiredOutput.clear();
	{
		int data = 20;
		const tCaPtr p(new Ca(&data));
		ASSERT_EQ(20, p->iData);
	}

	ASSERT_STREQ("CA::~CA()#", requiredOutput.c_str());
}

//! Testing ScopedPtr::CScopedPtr(const CScopedPtr&)
TEST(ScopedTest, ConstructorCopyScoped)
{
	tCaPtr p1(new Ca());
	tCaPtr p2(p1);
	ASSERT_EQ(0, p2->iData);
	//! Increment data of the p2
	p2->iData += 1;
	ASSERT_EQ(1, p2->iData);
}

//! Testing assignment operator
TEST(ScopedTest, Assign)
{
    tCaPtr p1(new Ca());
    p1 = new Ca(30);
    EXPECT_EQ(30, p1->iData);
}

//! Testing copy constructor CA::CA(const CA&)
TEST(ScopedTest, ConstructorCopy2)
{
    tCaPtr p1(new Ca(30));
    {
        tCaPtr p2(new Ca(*p1));
        ASSERT_EQ(30, p2->iData);
		//! Increment data of the p2
        p2->iData += 1;
        ASSERT_EQ(31, p2->iData);
    }
    ASSERT_EQ(30, p1->iData);
}
} // scoped