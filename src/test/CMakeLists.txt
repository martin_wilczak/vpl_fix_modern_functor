#################################################################################
# This file is part of
#
# VPL - Voxel Processing Library
# Copyright 2014 3Dim Laboratory s.r.o.
# All rights reserved.
#
# Use of this source code is governed by a BSD-style license that can be
# found in the LICENSE file.
################################################################################

if( BUILD_VPLTESTS )
    ADD_SUBDIRECTORY(VPLTests)
endif()
IF ( BUILD_GTESTS )
    ADD_SUBDIRECTORY(GoogleTests)
endif()