//==============================================================================`
/* This file comes from MDSTk software and was modified for
 *
 * VPL - Voxel Processing Library
 * Changes are Copyright 2018 3Dim Laboratory s.r.o.
 * All rights reserved.
 *
 * Use of this source code is governed by a BSD-style license that can be
 * found in the LICENSE file.
 *
 * The original MDSTk legal notice can be found below.
 *
 * Medical Data Segmentation Toolkit (MDSTk)
 * Copyright (c) 2003-2008 by PGMed
 *
 * Authors: J�n Br�da brida@t3d.team
 * Date:    2018/05/04
 *
 * Description:
 * - Simple signals and callback invocation mechanism exploiting C++11 features.
 */

#include <limits>
#include <vector>

namespace vpl
{
namespace mod
{

//______________________________________________________________________________
inline SignalConnection::SignalConnection() : m_signal{nullptr}
{
}

//______________________________________________________________________________
inline SignalConnection::SignalConnection(const SignalConnection& arg) :
                                          m_id{arg.m_id},
                                          m_signal{arg.m_signal}
{
}

//______________________________________________________________________________
inline SignalConnection::SignalConnection(const Id id,
                                          void* const signal) :
                                          m_id{id},
                                          m_signal{signal}
{
}

//______________________________________________________________________________
inline SignalConnection::~SignalConnection()
{
}

//______________________________________________________________________________
inline SignalConnection& SignalConnection::operator =(const SignalConnection& rhs)
{
    m_id = rhs.m_id;
    m_signal = rhs.m_signal;

    return *this;
}

//______________________________________________________________________________
inline SignalConnection::Id SignalConnection::getConnectionId() const
{
    return m_id;
}

//______________________________________________________________________________
inline void* SignalConnection::getSignalPtr()
{
    return m_signal;
}

//______________________________________________________________________________
inline const void* SignalConnection::getSignalPtr() const
{
    return m_signal;
}

//______________________________________________________________________________
template <typename R, typename... Ts>
inline Signal<R (Ts...)>::Signal() : m_counter{std::numeric_limits<SignalConnection::Id>::min()}
{
}

//______________________________________________________________________________
template <typename R, typename... Ts>
inline Signal<R (Ts...)>::Signal(const Signal& arg) : m_counter{arg.m_counter},
                                                      m_connections{arg.m_connections}
{
}

//______________________________________________________________________________
template <typename R, typename... Ts>
inline Signal<R (Ts...)>::~Signal()
{
}

//______________________________________________________________________________
template <typename R, typename... Ts>
inline Signal<R (Ts...)>& Signal<R (Ts...)>::operator =(const Signal& rhs)
{
    m_counter = rhs.m_counter;
    m_connections = rhs.m_connections;

    return *this;
}

//______________________________________________________________________________
template <typename R, typename... Ts>
SignalConnection Signal<R (Ts...)>::operator +=(Function slot)
{
    return connect(slot);
}

//______________________________________________________________________________
template <typename R, typename... Ts>
SignalConnection Signal<R (Ts...)>::operator +=(const Slot& slot)
{
    return connect(slot);
}

//______________________________________________________________________________
template <typename R, typename... Ts>
Signal<R (Ts...)>& Signal<R (Ts...)>::operator -=(const SignalConnection& connection)
{
    disconnect(connection);

    return *this;
}

//______________________________________________________________________________
template <typename R, typename... Ts>
template <typename T>
Signal<R (Ts...)>& Signal<R (Ts...)>::operator -=(const T* const receiver)
{
    disconnect(receiver);

    return *this;
}

//______________________________________________________________________________
template <typename R, typename... Ts>
template <typename U>
typename std::enable_if_t<!std::is_void<U>::value, U>
Signal<R (Ts...)>::operator ()(Ts... args) const
{
    static_assert(std::is_default_constructible_v<R>,
                  "The return type R must be default-constructible!");

    Lock lock(const_cast<Signal&>(*this));

    ConnectionMap::const_iterator it = m_connections.cbegin(),
                                  end = m_connections.cend();
    while (it != end)
    {
        // check if this is the last iterator
        // (unordered_map does not have reverse iterators)
        ConnectionMap::const_iterator tmp = it;
        if (++tmp == end)
        {
            // end here to process the last slot with a return statement
            break;
        }

        if (!it->second.blocked)
        {
            it->second.slot(std::forward<Ts>(args)...);
        }

        ++it;
    }

    if (it != m_connections.cend() && !it->second.blocked)
    {
        return static_cast<R>(it->second.slot(std::forward<Ts>(args)...));
    }

    return R{};
}

//______________________________________________________________________________
template <typename R, typename... Ts>
template <typename U>
typename std::enable_if_t<std::is_void<U>::value>
Signal<R (Ts...)>::operator ()(Ts... args) const
{
    Lock lock(const_cast<Signal&>(*this));

    for (const auto& c : m_connections)
    {
        if (!c.second.blocked)
        {
            c.second.slot(std::forward<Ts>(args)...);
        }
    }
}

//______________________________________________________________________________
template <typename R, typename... Ts>
template <typename A>
R Signal<R (Ts...)>::operator ()(Ts... args, A agg) const
{
	static_assert(!isVoid<R>(), "Return type R cannot be void");

    Lock lock(const_cast<Signal&>(*this));

	std::vector<R> results;

    for (const auto& c : m_connections)
    {
        if (!c.second.blocked)
        {
            results.push_back(c.second.slot(std::forward<Ts>(args)...));
        }
    }

	return agg(std::move(results));
}

//______________________________________________________________________________
template <typename R, typename... Ts>
inline SignalConnection Signal<R (Ts...)>::connect(Function slot)
{
    return connect(Slot{slot});
}

//______________________________________________________________________________
template <typename R, typename... Ts>
template <typename T>
inline SignalConnection Signal<R (Ts...)>::connect(T* const receiver,
                                                   R (T::*slot)(Ts...))
{
	return connect(signal::makeSlot(receiver, slot));
}

//______________________________________________________________________________
template <typename R, typename... Ts>
template <typename T>
inline SignalConnection Signal<R (Ts...)>::connect(const T* const receiver,
                                                   R (T::*slot)(Ts...) const)
{
	return connect(signal::makeSlot(receiver, slot));
}

//______________________________________________________________________________
template <typename R, typename... Ts>
inline SignalConnection Signal<R (Ts...)>::connect(vpl::base::Functor<R (Ts...)> functor)
{
    return connect(functor.getImpl());
}

//______________________________________________________________________________
template <typename R, typename... Ts>
inline void Signal<R (Ts...)>::disconnectAll()
{
    Lock lock(*this);

	m_connections.clear();
}

//______________________________________________________________________________
template <typename R, typename... Ts>
inline void Signal<R (Ts...)>::disconnect(const SignalConnection& connection)
{
    if (connection.getSignalPtr() == this)
    {
        Lock lock(*this);

        m_connections.erase(connection.getConnectionId());
    }
}

//______________________________________________________________________________
template <typename R, typename... Ts>
template <typename T>
inline void Signal<R (Ts...)>::disconnect(const T* const receiver)
{
    if (receiver) // let's not do this with nullptr
    {
        Lock lock(*this);

        for (ConnectionMap::iterator it =  m_connections.begin();
             it != m_connections.end();)
        {
            if (it->second.receiver == receiver)
            {
                it = m_connections.erase(it);
            }
            else
            {
                ++it;
            }
        }
    }
}

//______________________________________________________________________________
template <typename R, typename... Ts>
inline void Signal<R (Ts...)>::blockAll()
{
    Lock lock(*this);

    for (auto it : m_connections)
    {
        it.second.blocked = true;
    }
}

//______________________________________________________________________________
template <typename R, typename... Ts>
inline void Signal<R (Ts...)>::block(const SignalConnection connection)
{
    if (connection.getSignalPtr() == this)
    {
        Lock lock(*this);

        ConnectionMap::iterator it = m_connections.find(connection.getConnectionId());
        if (it != m_connections.end())
        {
            it->second.blocked = true;
        }
    }
}

//______________________________________________________________________________
template <typename R, typename... Ts>
inline void Signal<R (Ts...)>::block(const void* const receiver)
{
    if (receiver) // let's not do this with nullptr
    {
        Lock lock(*this);

        for (ConnectionMap::iterator it =  m_connections.begin();
             it != m_connections.end();
             ++it)
        {
            if (it->second.receiver == receiver)
            {
                it->second.blocked = true;
            }
        }
    }
}

//______________________________________________________________________________
template <typename R, typename... Ts>
inline void Signal<R (Ts...)>::blockAllButThis(const SignalConnection connection)
{
    if (connection.getSignalPtr() != this)
    {
        return;
    }

    Lock lock(*this);

    for (auto& c : m_connections)
    {
        if (c.first != connection.getConnectionId())
        {
            c.second.blocked = true;
        }
    }
}

//______________________________________________________________________________
template <typename R, typename... Ts>
inline void Signal<R (Ts...)>::unblockAll()
{
    Lock lock(*this);

    for (auto it : m_connections)
    {
        it.second.blocked = false;
    }
}

//______________________________________________________________________________
template <typename R, typename... Ts>
inline void Signal<R (Ts...)>::unblock(const SignalConnection connection)
{
    Lock lock(*this);

    if (connection.getSignalPtr() == this)
    {
        ConnectionMap::iterator it = m_connections.find(connection.getConnectionId());
        if (it != m_connections.end())
        {
            it->second.blocked = false;
        }
    }
}

//______________________________________________________________________________
template <typename R, typename... Ts>
inline void Signal<R (Ts...)>::unblock(const void* const receiver)
{
    if (receiver) // let's not do this with nullptr
    {
        Lock lock(*this);

        for (ConnectionMap::iterator it =  m_connections.begin();
             it != m_connections.end();
             ++it)
        {
            if (it->second.receiver == receiver)
            {
                it->second.blocked = false;
            }
        }
    }
}

//______________________________________________________________________________
template <typename R, typename... Ts>
bool Signal<R (Ts...)>::isBlocked(const SignalConnection connection) const
{
    if (connection.getSignalPtr() == this)
    {
        Lock lock(*this);

        ConnectionMap::const_iterator it = m_connections.find(connection.getConnectionId());
        return it != m_connections.cend() ? it->second.blocked : false;
    }

    return false;
}

//______________________________________________________________________________
template <typename R, typename... Ts>
size_t Signal<R (Ts...)>::getNumOfConnections() const
{
    return m_connections.size();
}

//______________________________________________________________________________
template <typename R, typename... Ts>
inline Signal<R (Ts...)>::Slot::Slot(Function slot,
                                     const void* const receiver) :
                                     slot{slot},
                                     receiver{receiver},
                                     blocked{false}
{
}

//______________________________________________________________________________
template <typename R, typename... Ts>
SignalConnection Signal<R (Ts...)>::connect(const Slot& slot)
{
    Lock lock(*this);

    return {m_connections.insert({m_counter++, slot}).first->first, this};
}


namespace signal
{

//______________________________________________________________________________
template <typename T, typename R, typename... Ts>
typename Signal<R (Ts...)>::Slot makeSlot(T* const receiver,
                                          R (T::*slot)(Ts...))
{
    return {[receiver = receiver, slot = slot] (Ts... args) -> R
            { return static_cast<R>((receiver->*slot)(args...)); },
            receiver};
}

//______________________________________________________________________________
template <typename T, typename R, typename... Ts>
typename Signal<R (Ts...)>::Slot makeSlot(const T* const receiver,
                                          R (T::*slot)(Ts...) const)
{
    return {[receiver = receiver, slot = slot] (Ts... args) -> R
            { return static_cast<R>((receiver->*slot)(args...)); },
            receiver};
}

}// namespace signal

//______________________________________________________________________________
template <typename R, typename... Ts>
inline SignalWrapper<R, Ts...>::SignalWrapper() : Signal{}
{
}

//______________________________________________________________________________
template <typename R, typename... Ts>
inline SignalWrapper<R, Ts...>::SignalWrapper(const SignalWrapper& arg) :
                                              Signal{arg}
{
}

//______________________________________________________________________________
template <typename R, typename... Ts>
inline SignalWrapper<R, Ts...>::~SignalWrapper()
{
}

//______________________________________________________________________________
template <typename R, typename... Ts>
SignalWrapper<R, Ts...>& SignalWrapper<R, Ts...>::operator =(SignalWrapper& rhs)
{
    Signal::operator =(rhs);

    return *this;
}

//______________________________________________________________________________
template <typename R, typename... Ts>
inline void SignalWrapper<R, Ts...>::invoke(Ts... args)
{
    Signal::operator ()(args...);
}

//______________________________________________________________________________
template <typename R, typename... Ts>
R SignalWrapper<R, Ts...>::invoke2(Ts... args)
{
    return Signal::operator ()(args...);
}

//______________________________________________________________________________
template <typename R, typename... Ts>
template <typename T>
SignalConnection SignalWrapper<R, Ts...>::connect(T& receiver, R (T::*slot)(Ts...))
{
    return connect(&receiver, slot);
}

//______________________________________________________________________________
template <typename R, typename... Ts>
template <typename T>
SignalConnection SignalWrapper<R, Ts...>::connect(const T& receiver, R (T::*slot)(Ts...) const)
{
    return connect(&receiver, slot);
}

//______________________________________________________________________________
template <typename R, typename... Ts>
SignalConnection SignalWrapper<R, Ts...>::connect(vpl::base::FunctorWrapper<R, Ts...> functor)
{
    return connect(functor.getImpl());
}

}// namespace mod
}// namespace vpl
