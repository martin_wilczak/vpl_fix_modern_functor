//==============================================================================
/* This file comes from MDSTk software and was modified for
 * 
 * VPL - Voxel Processing Library
 * Changes are Copyright 2014 3Dim Laboratory s.r.o.
 * All rights reserved.
 * 
 * Use of this source code is governed by a BSD-style license that can be
 * found in the LICENSE file.
 * 
 * The original MDSTk legal notice can be found below.
 * 
 * Medical Data Segmentation Toolkit (MDSTk) 
 * Copyright (c) 2003-2006 by Michal Spanel  
 *
 * Author:  Michal Spanel, spanel@fit.vutbr.cz  \n
 * Date:    2006/08/09                       
 *
 * Description:
 * - Image forward and backward FFT (Fast Fourier Transform).
 */

#ifndef VPL_IMAGEFFT_H
#define VPL_IMAGEFFT_H


//==============================================================================
/*
 * Include predefined image functions.
 */

// Image FFT
#include "ImageFunctions/FFT.h"


#endif // VPL_IMAGEFFT_H

