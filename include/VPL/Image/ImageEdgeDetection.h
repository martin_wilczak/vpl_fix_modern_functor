//==============================================================================
/* This file comes from MDSTk software and was modified for
 * 
 * VPL - Voxel Processing Library
 * Changes are Copyright 2014 3Dim Laboratory s.r.o.
 * All rights reserved.
 * 
 * Use of this source code is governed by a BSD-style license that can be
 * found in the LICENSE file.
 * 
 * The original MDSTk legal notice can be found below.
 * 
 * Medical Data Segmentation Toolkit (MDSTk) 
 * Copyright (c) 2003-2005 by Michal Spanel  
 *
 * Author:  Michal Spanel, spanel@fit.vutbr.cz  \n
 * Date:    2005/09/20                       
 * 
 * Description:
 * - Predefined image edge detectors.
 */

#ifndef VPL_ImageEdgeDetection_H
#define VPL_ImageEdgeDetection_H


//==============================================================================
/*
 * Include all predefined edge detectors.
 */

// General edge detectors
#include "EdgeDetection/Canny.h"
#include "EdgeDetection/ZeroCrossings.h"


#endif // VPL_ImageEdgeDetection_H

