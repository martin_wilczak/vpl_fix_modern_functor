rem Script which you can use to set Windows environment variables.
rem Don't hesitate to modify it...

set TRIDIM=d:\3Dim

set VPL=%TRIDIM%\VPL
set VPL_BIN=%VPL%\build\bin;%VPL%\scripts
set VPL_DATA=%VPL%\data
set VPL_TEMP=%VPL%\temp

set PATH=%VPL_BIN%;%PATH%

rem cd %VPL_TEMP%
cmd
