# CMake module that tries to find TinyXML includes and libraries.
#
# The following variables are set if TinyXML is found:
#   TINYXML_FOUND         - True when the TinyXML include directory is found.
#   TINYXML_INCLUDE_DIR   - The directory where TinyXML include files are.
#   TINYXML_LIBRARIES_DIR - The directory where TinyXML libraries are.
#   TINYXML_LIBRARIES     - List of all TinyXML libraries.
#
# Usage:
#   In your CMakeLists.txt file do something like this:
#   ...
#   FIND_PACKAGE(TinyXML)
#   ...
#   INCLUDE_DIRECTORIES(${TINYXML_INCLUDE_DIR})
#   LINK_DIRECTORIES(${TINYXML_LIBRARIES_DIR})
#   ...
#   TARGET_LINK_LIBRARIES( mytarget ${TINYXML_LIBRARIES} )


# Initialize the search path
if( WIN32 )
  set( TINYXML_DIR_SEARCH
       "${TRIDIM_3RDPARTY_DIR}"
       "${TRIDIM_MOOQ_DIR}"
       "${TINYXML_ROOT_DIR}"
       "$ENV{TINYXML_ROOT_DIR}"
       )
  set( TINYXML_SEARCHSUFFIXES
       include
       )
else( WIN32 )
  set( TINYXML_DIR_SEARCH
       "${TRIDIM_MOOQ_DIR}"
       "${TINYXML_ROOT_DIR}"
       /usr/include
       /usr/local/include
       /opt/include
       /opt/local/include
       )
  set( TINYXML_SEARCHSUFFIXES
       ""
       )
endif( WIN32 )


# Try to find TINYXML include directory
find_path( TINYXML_INCLUDE_DIR
           NAMES
           tinyxml2.h
           PATHS
           ${TINYXML_DIR_SEARCH}
           PATH_SUFFIXES
           ${TINYXML_SEARCHSUFFIXES}
           )


# Assume we didn't find it
set( TINYXML_FOUND NO )
# Now try to get the library path
IF( TINYXML_INCLUDE_DIR )

  # Look for the TINYXML library path
  SET( TINYXML_LIBRARIES_DIR ${TINYXML_INCLUDE_DIR} )

  # Strip off the trailing "/include" in the path
  IF( "${TINYXML_LIBRARIES_DIR}" MATCHES "/include$" )
    GET_FILENAME_COMPONENT( TINYXML_LIBRARIES_DIR ${TINYXML_LIBRARIES_DIR} PATH )
  ENDIF( "${TINYXML_LIBRARIES_DIR}" MATCHES "/include$" )

  # Check if the 'lib' directory exists
  IF( EXISTS "${TINYXML_LIBRARIES_DIR}/lib" )
    SET( TINYXML_LIBRARIES_DIR ${TINYXML_LIBRARIES_DIR}/lib )
  ENDIF( EXISTS "${TINYXML_LIBRARIES_DIR}/lib" )
    

  

  # Add unversioned image lib name to the testing list
  list(APPEND TINYXML_LIB_NAMES tinyxml2)
  list(APPEND TINYXML_LIB_DEBUG_NAMES tinyxml2d)
 
  

  # Find release libraries
  FIND_LIBRARY( TINYXML_LIBRARY
                NAMES ${TINYXML_LIB_NAMES}
                PATHS ${TINYXML_LIBRARIES_DIR} ${TINYXML_LIBRARIES_DIR_RELEASE}
                NO_DEFAULT_PATH NO_CMAKE_ENVIRONMENT_PATH
                NO_CMAKE_PATH NO_SYSTEM_ENVIRONMENT_PATH
                NO_CMAKE_SYSTEM_PATH )

  # Try to find debug libraries
  FIND_LIBRARY( TINYXML_DEBUG_LIBRARY
                NAMES ${TINYXML_LIB_DEBUG_NAMES}
                PATHS ${TINYXML_LIBRARIES_DIR} ${TINYXML_LIBRARIES_DIR_DEBUG}
                NO_DEFAULT_PATH NO_CMAKE_ENVIRONMENT_PATH
                NO_CMAKE_PATH NO_SYSTEM_ENVIRONMENT_PATH
                NO_CMAKE_SYSTEM_PATH )
  
  unset(TINYXML_LIB_NAMES)
  unset(TINYXML_LIB_DEBUG_NAMES)
  
               
  # Check what libraries was found
  IF( TINYXML_LIBRARY AND TINYXML_DEBUG_LIBRARY )
    SET( TINYXML_LIBRARIES
         optimized tinyxml2
         debug tinyxml2d)
    SET( TINYXML_FOUND "YES" )
    
  ELSE( TINYXML_LIBRARY AND TINYXML_DEBUG_LIBRARY )
 
    IF(TINYXML_LIBRARY)
      SET( TINYXML_LIBRARIES tinyxml2 )
      SET( TINYXML_FOUND "YES" )
      
    ELSE( TINYXML_LIBRARY )
    
      IF( TINYXML_libPythond_LIBRARY )
        SET( TINYXML_LIBRARIES  tinyxml2d)
        SET( TINYXML_FOUND "YES" )
      ENDIF( TINYXML_libPythond_LIBRARY )
      
    ENDIF( TINYXML_LIBRARY )
    
  ENDIF( TINYXML_LIBRARY AND TINYXML_DEBUG_LIBRARY )
  
ENDIF( TINYXML_INCLUDE_DIR )

# Using shared or static library?
IF( TINYXML_FOUND )
#    OPTION( TINYXML_USE_SHARED_LIBRARY "Should shared library be used?" OFF )
#    IF( TINYXML_USE_SHARED_LIBRARY )
#      ADD_DEFINITIONS( -DMDS_LIBRARY_SHARED )
#    ELSE( TINYXML_USE_SHARED_LIBRARY )
      ADD_DEFINITIONS( -DMDS_LIBRARY_STATIC )
#    ENDIF( TINYXML_USE_SHARED_LIBRARY )
    if( APPLE )
        add_definitions( -D_MACOSX )
    else()
        if( UNIX )
            add_definitions( -D_LINUX )
        endif()
    endif()
ENDIF( TINYXML_FOUND )

MARK_AS_ADVANCED( TINYXML_INCLUDE_DIR
                  TINYXML_LIBRARY
                  TINYXML_DEBUG_LIBRARY )


# display help message
IF( NOT TINYXML_FOUND )
  # make FIND_PACKAGE friendly
  IF( NOT TinyXML_FIND_QUIETLY )

    IF( TinyXML_FIND_REQUIRED )
      SET( TINYXML_ROOT_DIR "" CACHE PATH "TinyXML root dir")
      MESSAGE( FATAL_ERROR "TinyXML required but some headers or libs not found. Please specify it's location with TINYXML_ROOT_DIR variable.")
    ELSE( TinyXML_FIND_REQUIRED )
      MESSAGE(STATUS "ERROR: TinyXML was not found.")
    ENDIF( TinyXML_FIND_REQUIRED )
  ENDIF( NOT TinyXML_FIND_QUIETLY )
else()
unset(TINYXML_ROOT_DIR CACHE)
ENDIF()

