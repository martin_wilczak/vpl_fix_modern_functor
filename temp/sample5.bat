@echo off 
rem Please, modify and run the 'wsetenv.bat' batch file first in order to
rem setup path to built binaries, etc.

echo Creating volume data from DICOM slices...
rem del /q sample5.slcs
rem for %%i in (..\data\dicom\*.dcm) do mdsLoadDicom <%%i >>sample5.slcs
rem mdsMakeVolume <sample5.slcs >sample5.vlm
mdsLoadDicomDir -dir ..\data\dicom |mdsMakeVolume >sample5.vlm
mdsVolumeRange -auto <sample5.vlm |mdsVolumeView

echo Data filtering...
mdsVolumeFilter -filter anisotropic <sample5.vlm >sample5f.vlm
mdsVolumeRange -auto <sample5f.vlm |mdsVolumeView

echo Volume data segmentation using the FCM clustering technique...
mdsVolumeSegFCM -clusters 3 <sample5.vlm |mdsVolumeView -coloring segmented

