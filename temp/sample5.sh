#!/bin/sh
# Please, modify and run either the 'lsetenv.sh' or the 'msetenv.sh' shell
# script first in order to setup path to built binaries, etc.

echo "Create volume data from series of DICOM slices..."
# rm -f sample5.slcs
# for i in `ls -d ../data/dicom/*.dcm`;
# do
#     mdsLoadDicom <$i >>sample5.slcs
# done
# mdsMakeVolume <sample5.slcs >sample5.vlm
mdsLoadDicomDir -dir ../data/dicom |mdsMakeVolume >sample5.vlm
mdsVolumeRange -auto <sample5.vlm |mdsVolumeView

echo "Data filtering..."
#mdsVolumeFilter -filter gauss3 <sample5.vlm >sample5f.vlm
mdsVolumeFilter -filter anisotropic <sample5.vlm >sample5f.vlm
mdsVolumeRange -auto <sample5f.vlm |mdsVolumeView

echo "Volume data segmentation using the FCM clustering technique..."
mdsVolumeSegFCM -clusters 3 <sample5.vlm |mdsVolumeView -coloring segmented

