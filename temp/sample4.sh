#!/bin/sh
# Please, modify and run either the 'lsetenv.sh' or the 'msetenv.sh' shell
# script first in order to setup path to built binaries, etc.

echo "Filtering input image..."
mdsLoadDicom <../data/dicom/80.dcm |mdsSliceFilter -filter anisotropic >sample4.slc
mdsSliceRange -auto <sample4.slc |mdsSliceView

echo "Watershed transform..."
mdsSliceSegWatersheds -wsheds <sample4.slc |mdsSliceRange -auto |mdsSliceView

echo "Watershed segmentation..."
mdsSliceSegWatersheds -merge -simthr 0.1 <sample4.slc |mdsSliceView -coloring segmented
